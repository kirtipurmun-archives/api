using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KirtipurMun.Mpt.Api.Models
{
  public class Project : Trackable
  {
     public Guid Id { get; set; }
    [Required(AllowEmptyStrings=false)]
    public string Name { get; set; }
    [Display(Name="budget allocation")]
    public int BudgetAllocation { get; set; }
    [Display(Name="committee or tender organisation")]
    public Org Organisation { get; set; }
    [Display(Name="ward number")]
    public int WardNumber { get; set; }

    [Display(Name="scheduled start time")]
    public DateTime TimeStart { get; set; }

    [Display(Name="scheduled stop time")]
    public DateTime TimeStop { get; set; }
    [Display(Name="project's activities")]
    public List<Activity> Activities { get; set; } = new List<Activity>();
    [Required(AllowEmptyStrings=false)]
    public string Status { get; set; }
  }
}