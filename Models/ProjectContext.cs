using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.Extensions;

namespace KirtipurMun.Mpt.Api.Models
{
    public class ProjectContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Org> Orgs { get; set; }
        public DbSet<Rep> Reps { get; set; }
        public DbSet<Activity> Activities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
          string mysqlConnectionString = String.Format(
            "server={0};database={1};user={2};password={3}",
            Environment.GetEnvironmentVariable("MPTAPI_DB_SERVER"),
            Environment.GetEnvironmentVariable("MPTAPI_DB_DATABASE"),
            Environment.GetEnvironmentVariable("MPTAPI_DB_USER"),
            Environment.GetEnvironmentVariable("MPTAPI_DB_PASSWORD")
          );
          optionsBuilder.UseMySQL(mysqlConnectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          base.OnModelCreating(modelBuilder);

          modelBuilder.Entity<Activity>(entity =>
          {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Name).IsRequired();
            entity.Property(e => e.Status).IsRequired();
          });

          modelBuilder.Entity<Org>(entity =>
          {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Name).IsRequired();
            entity.HasMany(e => e.Reps);
          });
          
          modelBuilder.Entity<Project>(entity =>
          {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Name).IsRequired();
            entity.HasOne(e => e.Organisation);
            entity.HasMany(e => e.Activities);
            entity.Property(e => e.Status).IsRequired();
          });

          modelBuilder.Entity<Rep>(entity =>
          {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Name).IsRequired();
          });
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            var entries = ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                if (entry.Entity is Trackable trackable)
                {
                    var now = DateTime.UtcNow;
                    var user = GetCurrentUser();
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            trackable.LastUpdatedAt = now;
                            trackable.LastUpdatedBy = user;
                            break;

                        case EntityState.Added:
                            trackable.CreatedAt = now;
                            trackable.CreatedBy = user;
                            trackable.LastUpdatedAt = now;
                            trackable.LastUpdatedBy = user;
                            break;
                    }
                }
            }
        }

        private string GetCurrentUser()
        {
            return "Admin"; // TODO implement your own logic
        }
    }
}