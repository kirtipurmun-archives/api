using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KirtipurMun.Mpt.Api.Models
{
  public class Rep : Trackable
  {
    public Guid Id { get; set; }
    [Required(AllowEmptyStrings=false)]
    public string Name { get; set; }
    [Display(Name="phone number")]
    public string Phone { get; set; }
    [Display(Name="email address")]
    public string Email { get; set; }
  }
}