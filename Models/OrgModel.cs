using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KirtipurMun.Mpt.Api.Models
{
  public class Org
  {
    public Guid Id { get; set; }
    [Required(AllowEmptyStrings=false)]
    public string Name { get; set; }
    public List<Rep> Reps { get; set; } = new List<Rep>();
  }
}