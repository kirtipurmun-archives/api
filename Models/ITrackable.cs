using System;

namespace KirtipurMun.Mpt.Api.Models {
  public abstract class Trackable
  {
      public DateTime CreatedAt { get; set; }
      public string CreatedBy { get; set; }
      public DateTime LastUpdatedAt { get; set; }
      public string LastUpdatedBy { get; set; }
  }
}