using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KirtipurMun.Mpt.Api.Models
{
  public class Activity : Trackable
  {
    public Guid Id { get; set; }
    [Required]
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
    public int BudgetAllocation { get; set; }
    [Required]
    public string Status { get; set; }
  }
}