# ![Municipality Project Tracker](docs/images/mptlogo-light-vector.svg)
## RESTful API Webservice
This repository holds the source code for the [Municipality Project Tracker's](https://bitbucket.org/kirtipurmun/mpt) API subsystem application (or simply, the _MPT API_). It was developed by [Kirtipur Municipality](http://kirtipurmun.gov.np/) and is available for use and extension for any organisation that might benefit from it. 

This is an [ASP.NET Core 2.2](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.2) application that is written in C# and exposes a RESTful HTTP API for retrieving, storing and modifying project information from a MySQL Database.

## Features
MPT API has the following features for managing projects withing the municiplaity:

* Create projects, which are attributed to committees
* Manage the tasks that are required for the completion of a project
* Allocating spend and establishing schedules for delivery timing
* Manage committee membership, adding personnel along with contact information
* Notification emmission for reminders to approaching project deadlines
* HTTP/1.1 RESTful interface for storage, modification and retrieval of all data
* CORS enforcement for the prevention of XSS attacks

## Technologies
The API subsystem is built on the following technologies. In order to debug or extend the functionality of this application, you will need to have some familiarity of:

* [ASP.NET Core 2.2](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.2) — Microsoft's official multi-platform implementation of .NET
* [C# 8](https://docs.microsoft.com/en-us/dotnet/csharp/) — Microsoft's flagship object-oriented programming language.
* [EntityFrameworkCore (EFCore)](https://docs.microsoft.com/en-us/ef/core/) — Connects to a MySQL database using EFCore as an ORM solution.
* [MySQL EFCore Connector](https://dev.mysql.com/doc/connector-net/en/connector-net-entityframework-core.html) — The database connector to interface with the MySQL server and manage schema development and data migrations.

Additionally, the following servers are used to host the API application:

* [Docker](https://docker.com/) — An app container tool to help isolate services on a common infrastructure. Using Docker allows us to standardise the build environments across platforms.
* [MySQL 8](https://www.mysql.com/products/community/) — The RDBMS used by the API to store data.

## Development
This repository hosts the entire source code for developing and building the MPT API, the instructions of which are contained in this README file. It is highly recommended that you read the [Municipality Project Tracker's main README](https://bitbucket.org/kirtipurmun/mpt/src/master/README.md) to better understand how the MPT's subsystems work together to serve the MPT web application.

The following sections of this document will focus on how to set up your local development environment for development, debugging and testing this subsystem.

### Getting started
The best place to begin developing the Municipality Project Tracker is to establish a working version of the complete MPT web application. You may do this by following the instructions in the [main project repository for the MPT](https://bitbucket.org/kirtipurmun/mpt/src/master/README.md) on BitBucket. Following the instructions linked will allow you to download this repository, along with the [MPT AdminUI front-end subsystem](https://bitbucket.org/kirtipurmun/adminui/src) and Docker compose configuration for the MySQL service required by the API.

You may choose to develop the API in isolation by cloning only this repository, however there are no provisions for a MySQL instance within this repo. The instructions below will step through the process of clining the [main project repository for the MPT](https://bitbucket.org/kirtipurmun/mpt) and using it to:

1. Instantiate a MySQL server
2. Clone this API repository
3. Export the required environment variables
4. Run the API application

#### Prerequisites
As this is a ASP.NET Core 2.2 application, you will require the latest .NET Core 2.2.x SDK to be installed on your development machine. You may download this from the [.NET Downloads page from Microsoft](https://dotnet.microsoft.com/download/dotnet-core/2.2). Once installed and set up, we will be using the dotnet CLI application to download dependencies and watch for file changes between builds.

##### Docker (optional for serving MySQL locally)
MySQL is the database that the MPT API uses to store project and committee information.

For development purposes, this guide assumes that you will be using the MySQL server that is instantiated using Docker compose within the [Municipality Project Tracker's main README](https://bitbucket.org/kirtipurmun/mpt/src/master/README.md). This will require [Docker](https://docker.com/) to be installed on your local machine.

If you have access to an alternative MySQL database either locally or remotely, you may configure the development environment to connect to it, by configuring your environment variables accordingly (see below).

#### 1. Launch and prepare the MySQL database using Docker
You may skip this step if you have already have a MySQL instance that you plan to use. For the purposes of these instructions, we will be using the MySQL Docker container that is configured in the [Municipality Project Tracker's main README](https://bitbucket.org/kirtipurmun/mpt/src/master/README.md) (although you will not need to follow those instructions).

##### 1.1. Clone the MPT repository
To download the Docker compose configuration for our MySQL Database (and incidentally, both the AdminUI subsystem and this API subsystem), clone the [MPT repository](https://bitbucket.org/kirtipurmun/mpt) using git:

```
$ git clone https://kashikirtipurmun@bitbucket.org/kirtipurmun/mpt.git
```
Note: If you are going to be submitting changes to BitBucket, it is advised that you use SSH instead.

##### 1.2. Launch the MySQL Docker instance
Once the `mpt` repository has been cloned, we can use the Docker compose configuration in `mpt/docker-compose.yml` to launch an instance of MySQL 8 as a standalone container. Using this configuration, our MySQL instance will have a persistent data volume so that whatever is stored within MySQL will be preserved when the service is shut down.

To launch the MySQL server, execute the following commands:
```
# Firstly, enter into our MPT directory
$ cd mpt
$ docker-compose up db -d
```
Note: You may omit the `-d` if you would like to keep the log output of the MySQL server in your terminal or console window. This can be useful for troubleshooting or debugging.

Docker will download the latest official MySQL image, and configure it with the environment variables stored in `./env.mysql.development`. These environment variables are important, as they determine the database name, user name and password that the API will require to connect to the database.

#### 2. Initialise the API (and AdminUI) submodules
Cloning the [MPT repository](https://bitbucket.org/kirtipurmun/mpt) will place identifiers for both the API repository (this repository) and the [AdminUI repository](https://bitbucket.org/kirtipurmun/adminui/). They will need to be initialised and pulled seperately, which can be done using the following commands:

```
# From within the ./mpt directory:
$ git submodule init
$ git submodule update
```
This will download and update each of the repositories (including this one) into subdirectories within the `mpt` folder in which we cloned the MPT repository.

#### 3. Export the environment variables required by the API
The API uses environment variables to determine which MySQL server to connect to, along with .NET Core runtime environment configuration and API application settings. Each of these environment variables and their purpose are decribed in the `.env.development` file within this repository. For the purpose of these instructions, we will use the defaults that are contained in the `.env.development` env file. To export these, execute the following command:

```
# From winin the ./mpt directory:
$ cd api
$ export $(egrep -v '^#' .env.development | xargs)
```

#### 4. Restore the ASP.NET Core application dependencies
With the environment variables in place, we can download the dependencies used by the API application using the `dotnet` CLI application. The following commands will install the dependiences ready for building and debugging:

```
# From within the ./mpt/api directory:
$ dotnet restore
```

#### 5. Run the ASP.NET API application
Finally, we can run the API application and watch for file changes to trigger a rebuild using `dotnet` CLI. To launch this application, run the following:

```
# From within the ./mpt/api directory:
$ dotnet watch run
```

Once established, you should be able to access the API endpoints on the default port, using the browser or REST navigator of your choice.

### Postman collection
This repository contains a collection of REST configurations for [Postman](https://www.getpostman.com), which should make querying the API easier once the application is running. You will find the MPT API collection saved in this repository's `docs` directory in JSON format. Use the _Import..._ option from within Postman to use this collection.

# License
MIT License

Copyright (c) 2019 Kirtipur Municipality

You may use and extend this software for whatever purpose you desire, as long as you leave the copyrights and acknowledgements in place. See LICENCE for details.