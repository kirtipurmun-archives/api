using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KirtipurMun.Mpt.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirtipurMun.Mpt.Api.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class RepsController : ControllerBase
  {
    [HttpGet]
    public ActionResult<IEnumerable<Rep>> Get()
    {
      using(var projectContext = new ProjectContext()) {
        var reps = projectContext.Reps;
        return reps.ToList();
      }
    }

    [HttpPost]
    public ActionResult<Rep> Post(Rep repBody) {
      if (!ModelState.IsValid) {
        return null;
      }
      using(var context = new ProjectContext()) {
        context.Reps.Add(repBody);
        context.SaveChanges();
        return repBody;
      }
    }

  }
}