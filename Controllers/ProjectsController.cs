using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KirtipurMun.Mpt.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirtipurMun.Mpt.Api.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class ProjectsController : ControllerBase
  {

    [HttpGet]
    public ActionResult<IEnumerable<Project>> Get()
    {
      using(var projectContext = new ProjectContext()) {
        IQueryable<Project> projects = projectContext.Projects;
        return projects.ToList();
      }
    }

    [HttpGet]
    [Route("with-activities")]
    public ActionResult<IEnumerable<Project>> GetWithActivities()
    {
      using(var projectContext = new ProjectContext()) {
        IQueryable<Project> projects = projectContext.Projects
                                        .Include(p => p.Activities);
        return projects.ToList();
      }
    }

    [HttpGet]
    [Route("{projectId}")]
    public async Task<ActionResult<Project>> Get(Guid projectId)
    {
      using(var context = new ProjectContext()) {
        var project = await context.Projects.Include(p => p.Activities)
                            .FirstOrDefaultAsync(p => p.Id == projectId);
        if (project == null) {
          return NotFound();
        }
        return project;
      }
    }

    [HttpPut]
    [Route("{projectId}")]
    public async Task<ActionResult<Project>> Put(Guid projectId, Project updatedProject)
    {
      using(var context = new ProjectContext()) {
        var project = context.Projects.Include(p => p.Activities).FirstOrDefault(p => p.Id == projectId);

        if (project == null) {
          return NotFound();
        }

        await UpdateActivities(project, updatedProject.Activities);
        project.Name = updatedProject.Name;
        project.Status = updatedProject.Status;
        project.WardNumber = updatedProject.WardNumber;
        
        await context.SaveChangesAsync();
        return project;
      }
    }

    private async Task<List<Activity>> UpdateActivities(Project project, List<Activity> newActivities)
    {
      using(var context = new ProjectContext()) {
        var oldActivities = project.Activities;
        var activitiesToRemove = oldActivities.Where(p => !newActivities.Any(uP => p.Id == uP.Id));
        var activitiesToUpdate = newActivities.Where(a => a.Id != Guid.Empty);
        var activitiesToAdd = newActivities.Where(a => a.Id == Guid.Empty);

        context.Activities.RemoveRange(activitiesToRemove);
        project.Activities.AddRange(activitiesToAdd);

        foreach (Activity updatedActivity in activitiesToUpdate)
        {
          var activity = await context.Activities.FirstOrDefaultAsync(a => a.Id == updatedActivity.Id);
          if (activity == null) continue;
          if (activity.Equals(updatedActivity)) continue;

          activity.Name = updatedActivity.Name;
          activity.Description = updatedActivity.Description;
          activity.Status = updatedActivity.Status;
          activity.BudgetAllocation = updatedActivity.BudgetAllocation;
          activity.Start = updatedActivity.Start;
          activity.End = updatedActivity.End;
        }
        await context.SaveChangesAsync();
        return project.Activities;
      }
    }

    [HttpPost]
    public ActionResult<Project> Post(Project projectBody) {
      if (!ModelState.IsValid) {
        return null;
      }
      using(var context = new ProjectContext()) {
        context.Projects.Add(projectBody);
        context.SaveChanges();
        return projectBody;
      }
    }

    [HttpDelete]
    [Route("{projectId}")]
    public ActionResult<Project> Delete(Guid projectId) {
      using (var context = new ProjectContext()) {
        var projectSearch = context.Projects.Where(p => p.Id == projectId);
        if (projectSearch.Count() == 0) {
          return NotFound();
        }
        var project = projectSearch.First();
        context.Remove(project);
        context.SaveChanges();
        return project;
      }
    }

    [HttpPost]
    [Route("{projectId}/add-activity")]
    public ActionResult<Project> AddActivity(Guid projectId, Activity activity) {
      if (!ModelState.IsValid) {
        return null;
      }

      using(var context = new ProjectContext()) {
        var project = context.Projects
              .Where(p => p.Id == projectId)
              .Include(p => p.Activities)
              .First();

        if (project == null) {
          return NotFound();
        }

        if (project.Activities.Exists(
          p => p.Name == activity.Name
          && p.Description == activity.Description
          && p.Status == activity.Status
          )) {
          return Conflict();
        }

        project.Activities.Add(activity);
        context.SaveChanges();
        return project;
      }
    }
  }
}