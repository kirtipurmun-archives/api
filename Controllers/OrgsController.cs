using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KirtipurMun.Mpt.Api.Models;
using Organisation = KirtipurMun.Mpt.Api.Models.Org;

namespace KirtipurMun.Mpt.Api.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class OrgsController : ControllerBase
  {
    [HttpGet]
    public ActionResult<IEnumerable<Organisation>> Get()
    {
      using(var projectContext = new ProjectContext()) {
        var Orgs = projectContext.Orgs
                    .Include(o => o.Reps);
        return Orgs.ToList();
      }
    }

    [HttpPost]
    public ActionResult<Organisation> Post(Organisation OrgBody) {
      if (!ModelState.IsValid) {
        return null;
      }
      using(var context = new ProjectContext()) {
        context.Orgs.Add(OrgBody);
        context.SaveChanges();
        return OrgBody;
      }
    }

    [HttpPost]
    [Route("assign-rep/{orgId}/{repId}")]
    public ActionResult<Organisation> AddRep(Guid orgId, Guid repId)
    {
      Organisation org;
      Rep rep;
      using (var context = new ProjectContext()) {
        var orgSearch = context.Orgs.Where(p => p.Id == orgId)
                          .Include(o => o.Reps);
        if (orgSearch.Count() == 0) {
          return NotFound();
        }
        org = orgSearch.First();
        var repSearch = context.Reps.Where(p => p.Id == repId);
        if (repSearch.Count() == 0) {
          return NotFound();
        }
        rep = repSearch.First();

        if (org.Reps.Exists(r => r.Id == rep.Id)) {
          return Conflict();
        }

        org.Reps.Add(rep);
        context.SaveChanges();
        return org; 
      }
    }

  }
}