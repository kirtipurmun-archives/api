using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KirtipurMun.Mpt.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirtipurMun.Mpt.Api.Controllers
{
  [Route("[controller]")]
  [ApiController]
  public class ActivitiesController : ControllerBase
  {
    [HttpGet]
    public ActionResult<IEnumerable<Activity>> Get()
    {
      using(var context = new ProjectContext()) {
        var activities = context.Activities;
        return activities.ToList();
      }
    }

    [HttpPost]
    public ActionResult<Activity> Post(Activity activity) {
      if (!ModelState.IsValid) {
        return BadRequest();
      }
      using(var context = new ProjectContext()) {
        context.Activities.Add(activity);
        context.SaveChanges();
        return activity;
      }
    }
  }
}