﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KirtipurMun.Mpt.Api.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KirtipurMun.Mpt.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateDatabase();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        protected static void CreateDatabase()
        {
          using (var context = new ProjectContext()) {
            context.Database.EnsureCreated();
          }
        }
    }
}
